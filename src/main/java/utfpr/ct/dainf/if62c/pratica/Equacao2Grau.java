package utfpr.ct.dainf.if62c.pratica;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Gliceu
 */
public class Equacao2Grau< T extends Number & Comparable> {

    private T a, b, c;
    private static final String MSG_COEF_ZERO = "\"Coeficiente a não pode ser zero\"";
    private static final String MSG_RAIZ_IMAG = "\"Equação não tem solução real\"";

    public Equacao2Grau() {
    }

    public Equacao2Grau(T a, T b, T c) {
         if (a.doubleValue() == 0) {
            throw new RuntimeException(MSG_COEF_ZERO);
        }
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public T getA() {
        return a;
    }

    public void setA(T a) {
        // gerar exceção se a for igual a zero.
       

        this.a = a;
    }

    public T getB() {
        return b;
    }

    public void setB(T b) {
        this.b = b;
    }

    public T getC() {
        return c;
    }

    public void setC(T c) {
        this.c = c;
    }

    public double getDeltaVer() {
        double eq1 = Math.pow(b.doubleValue(), 2) - 4 * a.doubleValue() * c.doubleValue();

        return eq1;
    }

    public double getRaiz1() {
        // gerar exceção se a raiz1 for imaginária.
        double delta = Math.sqrt(Math.pow(b.doubleValue(), 2) - 4 * a.doubleValue() * c.doubleValue());
        double deltaVer = Math.pow(b.doubleValue(), 2) - 4 * a.doubleValue() * c.doubleValue();
        if (deltaVer < 0) {
            throw new RuntimeException(MSG_RAIZ_IMAG);
        }
        return (-b.doubleValue() + delta) / (2 * a.doubleValue());
    }

    public double getRaiz2() {
        // gerar exceção se a raiz2 for imaginária.
        double delta = Math.sqrt(Math.pow(b.doubleValue(), 2) - 4 * a.doubleValue() * c.doubleValue());
        double deltaVer = Math.pow(b.doubleValue(), 2) - 4 * a.doubleValue() * c.doubleValue();
        if (delta < 0) {
            throw new RuntimeException(MSG_RAIZ_IMAG);
        }

        return (-b.doubleValue() - delta) / (2 * a.doubleValue());
    }
}
