package utfpr.ct.dainf.if62c.pratica;
//@author Gliceu Camargo

public class MatrizInvalidaException extends Exception {

    public int numlinhas;
    public int numcolunas;

    public MatrizInvalidaException() {
        super(String.format(
                "\"Matriz de 5x0 não pode ser criada\""));

    }

    public int getNumLinhas(int l) throws MatrizInvalidaException {
        if (l <= 0) {
            throw new MatrizInvalidaException();
        }
        return numlinhas;
    }

    public int getNumColunas(int c) throws MatrizInvalidaException {
        if (c <= 0) {
            throw new MatrizInvalidaException();
        }
        return numcolunas;
    }

}
