
import utfpr.ct.dainf.if62c.pratica.Matriz;
import utfpr.ct.dainf.if62c.pratica.MatrizInvalidaException;
import utfpr.ct.dainf.if62c.pratica.ProdMatrizesIncompativeisException;
import utfpr.ct.dainf.if62c.pratica.SomaMatrizesIncompativeisException;

// @author Gliceu Camargo <gliceu@utfpr.edu.br>
public class Pratica51 {

    public static void main(String[] args) throws Exception {
try{
        Matriz orig = new Matriz(3, 3);
        double[][] m = orig.getMatriz();
        m[0][0] = 6;
        m[0][1] = 2;
        m[0][2] = 2;
        m[1][0] = 3;
        m[1][1] = 4;
        m[1][2] = 2;
        m[2][0] = 5;
        m[2][1] = 6;
        m[2][2] = 2;
        Matriz orig1 = new Matriz(3, 3);
        double[][] n = orig1.getMatriz();
        n[0][0] = 2;
        n[0][1] = 2;
        n[0][2] = 2;
        n[1][0] = 3;
        n[1][1] = 4;
        n[1][2] = 2;
        n[2][0] = 5;
        n[2][1] = 7;
        n[2][2] = 2;
        Matriz transp = orig.getTransposta();
        Matriz soma = orig.soma(orig, orig1);
        Matriz produto = orig.prod(orig, orig1);
        System.out.println("Matriz original  : " + orig);
        System.out.println("Matriz transposta: " + transp);
        System.out.println("Matriz soma      : " + soma);
        System.out.println("Matriz produto   : " + produto);
     } 
          catch(SomaMatrizesIncompativeisException e){System.out.println(e.toString());}
          catch(ProdMatrizesIncompativeisException e){System.out.println(e.toString());}

        try {
            MatrizInvalidaException testa = new MatrizInvalidaException();
            testa.getNumLinhas(5);
            testa.getNumColunas(0);
        } catch (MatrizInvalidaException e) {
            System.out.printf("%s\n", e.toString());
        }

        Matriz orig2 = new Matriz(3, 1);
        Matriz orig3 = new Matriz(5, 3);
        try{
        SomaMatrizesIncompativeisException somaMat = new SomaMatrizesIncompativeisException(orig2, orig3);
        Matriz a = somaMat.soma(somaMat.getM1(), somaMat.getM2());
        }
        catch(SomaMatrizesIncompativeisException e){System.out.printf("%s\n",e.toString());}
        try{
        ProdMatrizesIncompativeisException prodMat = new ProdMatrizesIncompativeisException(orig2, orig3);
        Matriz b = prodMat.prodMat(prodMat.getM1(), prodMat.getM2());}
        catch(ProdMatrizesIncompativeisException e){System.out.println("\"Matrizes de 3x1 e 5x3 não podem ser multiplicadas\"");}
        
    }
}
